package com.alberto.reactangulo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {

    private TextView lbNombre;
    private EditText txtBase;
    private EditText txtAltura;
    private EditText txtArea;
    private EditText txtPerimetro;
    private Button btnCalcular;
    private Button btnRegresar;
    private Button btnLimpiar;
    private Rectangulo rectangulo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        lbNombre = findViewById(R.id.lbNombre);
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        txtArea = findViewById(R.id.txtArea);
        txtPerimetro = findViewById(R.id.txtPerimetro);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        rectangulo = new Rectangulo();
        String nombre = getIntent().getStringExtra("nombre");
        lbNombre.setText(String.format("Hola %s", nombre));

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtBase.getText().toString().matches("")|| txtAltura.getText().toString().matches(""))
                {
                    Toast.makeText(getApplicationContext(), "No puedes dejar los campos vacios", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    rectangulo.setAltura(Integer.parseInt(txtAltura.getText().toString()));
                    rectangulo.setBase(Integer.parseInt(txtBase.getText().toString()));
                    txtPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
                    txtArea.setText(String.valueOf(rectangulo.calcularArea()));
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtArea.setText("");
                txtBase.setText("");
                txtPerimetro.setText("");

            }
        });
    }
}
